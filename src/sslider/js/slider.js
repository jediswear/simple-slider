import '../styles/slider.scss'

const _slider= new WeakMap()
const _slides = new WeakMap()
const _renderSlides = new WeakMap()
const _animate = new WeakMap()
const _createControls = new WeakMap()
const _createSliderLayout = new WeakMap()
const _addListenersToControls = new WeakMap()

class Slider {
    constructor(selector){
        _slider.set(this, document.querySelector(selector))
        _slides.set(this, createSlides(_slider.get(this).querySelectorAll('img')))

        _renderSlides.set(this, () => {
            const moveContainer = _slider.get(this).querySelector('.moveSlides')

            moveContainer.removeAttribute('style')
            moveContainer.innerHTML = ''

            _slides.get(this).forEach(slide => moveContainer.appendChild(slide))
        })
        _animate.set(this, (direction) => {
            const moveContainer = _slider.get(this).querySelector('.moveSlides')

            const fps = 60
            const time = 600

            const maxIteration = Math.floor(time / fps)
            const step = (100 / 4) / maxIteration
            let currIteration = 0

            const timer = setInterval(() => {
                if(currIteration >= maxIteration - 1){
                    clearInterval(timer)

                    setTimeout(() => {
                        _renderSlides.get(this)()
                    }, 1000/fps)
                }

                currIteration++

                moveContainer.style = `${direction}: ${currIteration * step}%`
            }, 1000 / fps)
        })
        _createControls.set(this, () => {
            const controlBtnContainer = document.createElement('div')
            controlBtnContainer.classList.add('controls')
            controlBtnContainer.innerHTML = `<span class="prev">
                                            <i class="fas fa-arrow-circle-left"></i>
                                        </span>
                                        <span class="next">
                                            <i class="fas fa-arrow-circle-right"></i>
                                        </span>`

            _slider.get(this).appendChild(controlBtnContainer)
        })
        _addListenersToControls.set(this, () => {
            const prevBtn = _slider.get(this).querySelector('.prev')
            const nextBtn = _slider.get(this).querySelector('.next')

            prevBtn.addEventListener('click', () => this.prev())
            nextBtn.addEventListener('click', () => this.next())
        })
        _createSliderLayout.set(this, () => {
            const slidesContainer = document.createElement('div')
            const moveContainer = document.createElement('div')

            slidesContainer.classList.add('slides')
            moveContainer.classList.add('moveSlides')

            slidesContainer.appendChild(moveContainer)
            _slider.get(this).appendChild(slidesContainer)
        })
    }

    init(){
        _slider.get(this).innerHTML = ''
        _createSliderLayout.get(this)()
        _renderSlides.get(this)()
        _createControls.get(this)()
        _addListenersToControls.get(this)()
    }

    next(){
        const first = _slides.get(this)[0]

        _slides.get(this).splice(0, 1)
        _slides.get(this).push(first)

        _animate.get(this)('right')
    }

    prev(){
        const last = _slides.get(this)[_slides.get(this).length - 1]

        _slides.get(this).splice(_slides.get(this).length - 1, 1)
        _slides.get(this).unshift(last)

        _animate.get(this)('left')
    }
}

const createSlides = function (imgNodList) {
    const res = []

    imgNodList.forEach(img => {
        const container = document.createElement('div')
        container.classList.add('slide')
        container.appendChild(img.cloneNode(true))

        res.push(container)
    })

    return res
}

export default Slider